import React, { Component } from "react";

class HistoryList extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="container history-list">
          <div className="row">
            <div
              className="col-1 history-status"
              style={{ backgroundColor: "#4dbd74" }}
            >
              <h2>
                <i className="fa fa-check" />
              </h2>
            </div>
            <div className="col">
              <div className="row">
                <div className="col-12">{this.props.history.end_location}</div>
                <div className="col-12">{this.props.history.date}</div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default HistoryList;
