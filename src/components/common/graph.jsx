import React, { Component } from "react";
import { Line } from "react-chartjs-2";

class Graph extends Component {
  getDatas() {
    if (this.props.data) {
      return this.props.data.map(m => m.count);
    }
  }

  getLabels() {
    if (this.props.data) {
      return this.props.data.map(m => m.label);
    }
  }
  render() {
    const data = canvas => {
      const ctx = canvas.getContext("2d");
      const gradient = ctx.createLinearGradient(0, 0, 100, 0);
      const datas = this.getDatas();
      const labels = this.getLabels();
      console.log(datas);
      return {
        labels: labels,
        datasets: [
          {
            label: "# of " + this.props.label,
            data: datas,
            backgroundColor: "rgba(" + this.props.color + ",1)",
            borderWidth: 1
          }
        ],
        backgroundColor: gradient
      };
    };
    return (
      <div className="col-6">
        <div className="btn-group" role="group" aria-label="Basic example">
          <button
            onClick={this.props.onYearly}
            type="button"
            className="btn btn-secondary"
          >
            Yearly
          </button>
          <button
            onClick={this.props.onMonthly}
            type="button"
            className="btn btn-secondary"
          >
            Monthly
          </button>
          <button
            onClick={this.props.onWeekly}
            type="button"
            className="btn btn-secondary"
          >
            Weekly
          </button>
        </div>
        <Line data={data} />
      </div>
    );
  }
}

export default Graph;
