/*global google*/

import React, { Component } from "react";
import { toast, ToastContainer } from "react-toastify";

class UserOrder extends Component {
  componentDidMount() {
    this.props.onShow();
  }
  render() {
    const mapStyle = {
      width: "500",
      height: "850px"
    };
    return (
      <div>
        <ToastContainer />
        <div className="form-group searchbox col-5">
          <div className="input-group">
            <div className="input-group-prepend">
              <span className="input-group-text">
                <i style={{ color: "#8cbd47" }} className="fa fa-map-marker" />
              </span>
            </div>
            <input
              className="form-control col-12 search-input"
              id="start-input"
              ref={node => {
                this.start = node;
              }}
              onKeyPress={this.props.onOrigin}
              defaultValue={this.props.origin}
            />
          </div>
          <div className="input-group">
            <div className="input-group-prepend">
              <span className="input-group-text">
                <i style={{ color: "#edb541" }} className="fa fa-map-marker" />
              </span>
            </div>
            <input
              className="form-control col-12 search-input"
              id="end-input"
              ref={node => {
                this.end = node;
              }}
              onKeyUp={this.props.onDestination}
            />
          </div>
          {this.props.destination !== "" && (
            <div
              className="row container-fluid mt-2"
              id="order-information"
              style={{ backgroundColor: "white", padding: "15px" }}
            >
              <div className="col-12 col-md-6">
                <h4 style={{ margin: 0 }}>IDR {this.props.price}</h4>
              </div>
              <div className="col-12 col-md-6 text-right">
                <button
                  className="btn btn-success"
                  onClick={this.props.onOrder}
                >
                  ORDER NOW
                </button>
              </div>
            </div>
          )}
        </div>

        <div
          ref={node => {
            this.map = node;
          }}
          style={mapStyle}
        />
      </div>
    );
  }
}

export default UserOrder;
