/*global google*/

import React, { Component } from "react";
import { toast, ToastContainer } from "react-toastify";

class UserCurrentOrder extends Component {
  componentDidMount() {
    this.props.onShow();
  }
  render() {
    const mapStyle = {
      width: "500",
      height: "850px"
    };
    return (
      <div>
        <ToastContainer />
        <div>
          <div className="form-group current-order col-5">
            <div
              className="row container-fluid mt-2"
              id=""
              style={{ padding: "15px" }}
            >
              <div className="col-4">Logo</div>
              <div className="col-4 text-center">Order Number</div>
              <div className="col-4 text-center">Customer Name</div>
            </div>
            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">
                  <i
                    style={{ color: "#8cbd47" }}
                    className="fa fa-map-marker"
                  />
                </span>
              </div>
              <input
                className="form-control col-12 search-input"
                id="start-input"
                ref={node => {
                  this.start = node;
                }}
                defaultValue={this.props.currentOrigin}
                disabled
              />
            </div>
            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">
                  <i
                    style={{ color: "#edb541" }}
                    className="fa fa-map-marker"
                  />
                </span>
              </div>
              <input
                className="form-control col-12 search-input"
                id="end-input"
                ref={node => {
                  this.end = node;
                }}
                defaultValue={this.props.currentDestination}
                disabled
              />
            </div>

            <div
              className="row container-fluid"
              id="order-information"
              style={{ padding: "15px" }}
            >
              <div className="col-6">
                <h4 style={{ margin: 0 }}>IDR {this.props.currentPrice}</h4>
              </div>
              <div className="col-6 text-right">
                <button onClick={this.onDelete} className="btn btn-danger mr-2">
                  CANCELLED ORDER
                </button>
              </div>
            </div>
          </div>

          <div
            ref={node => {
              this.map = node;
            }}
            style={mapStyle}
          />
        </div>
      </div>
    );
  }
}
export default UserCurrentOrder;
