import React from "react";
import Joi from "joi-browser";
import Form from "../common/form";
import auth from "../../services/user/authService";
import { Redirect } from "react-router-dom";
import logo from "../../logo.png";

class AdminLogin extends Form {
  state = {
    data: { username: "", password: "" },
    errors: {}
  };

  schema = {
    username: Joi.string()
      .required()
      .label("Username"),
    password: Joi.string()
      .required()
      .label("Password")
  };

  doSubmit = async () => {
    try {
      const { data } = this.state;
      await auth.userLogin(data.username, data.password);
      const { state } = this.props.location;
      window.location = state ? state.from.pathname : "/user";
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.username = ex.response.data;
        this.setState({ errors });
      }
    }
  };

  render() {
    if (auth.getCurrentUser()) return <Redirect to="/user" />;
    return (
      <div className="container-fluid" id="content-wrapper">
        <div id="container-form">
          <img src={logo} style={{ width: "100%" }} alt="" srcset="" />
          <div className="form-wrapper text-center">
            <h3 id="form-title">SIGN IN FORM</h3>
            <form id="form" onSubmit={this.handleSubmit}>
              {this.renderInput("username", "Username")}
              {this.renderInput("password", "Password", "password")}
              {this.renderButton("login")}
              <h6>Don't have any account?</h6>
              <h6>
                <strong>Sign up</strong>
              </h6>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AdminLogin;
