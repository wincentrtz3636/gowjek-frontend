/*global google*/

import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import SideBar from "../common/Sidebar/sidebar";
import Navbar from "../common/Navbar/navbar";
import { toast, ToastContainer } from "react-toastify";
import { css } from "glamor";

import UserProfile from "./userProfile";
import UserOrder from "./userOrder";
import UserHistory from "./userHistory";
import UserCurrentOrder from "./userCurrentOrder";

import {
  getUserInfo,
  order,
  userCancelOrder,
  getUserOrders
} from "../../services/user/userService";
import socketIOClient from "socket.io-client";

import "./user.css";

class UserDashboard extends Component {
  state = {
    user: "User",
    color: {
      navbar: "#8cbd47",
      navbar_header: "#82aa49"
    },
    sidebar: [
      { label: "Dashboard", path: "/user" },
      { label: "Order", path: "/user/order" },
      { label: "Current", path: "/user/order/current" },
      { label: "History", path: "/user/history" }
    ],
    sidebarStatus: "",
    origin: "",
    destination: "",
    price: 0
  };

  constructor() {
    super();
    this.calculateAndDisplayRoute = this.calculateAndDisplayRoute.bind(this);
    this.calculateDistance = this.calculateDistance.bind(this);
    this.showMap = this.showMap.bind(this);
  }

  notify = () =>
    (this.toastId = toast("Finding Driver . . . .", {
      type: "info",
      autoClose: false
    }));

  update = (label, type) =>
    toast.update(this.toastId, {
      render: label,
      type: type,
      className: css({
        transform: "rotateY(360deg)",
        transition: "transform 0.6s"
      }),
      autoClose: 8000
    });

  async componentDidMount() {
    const { data } = await getUserInfo();
    const { data: orders } = await getUserOrders();
    if (orders[0].status_id < 3) {
      this.setState({
        currentOrigin: orders[0].start_location,
        currentDestination: orders[0].end_location,
        currentPrice: orders[0].price
      });
    }
    this.setState({ user: data });
  }

  showMap() {
    let self = this;
    loadScript(
      "https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyByH0c5bxYDZ48BLQ401BBsm4DppG6QNkQ&libraries=places",
      function() {
        self.child.directionsService = new google.maps.DirectionsService();
        self.child.directionsDisplay = new google.maps.DirectionsRenderer();
        self.child.map = new google.maps.Map(self.child.map, {
          zoom: 13,
          center: { lat: -25.363, lng: 131.044 }
        });

        self.child.startInput = self.child.start;
        self.child.searchBoxStart = new google.maps.places.SearchBox(
          self.child.startInput
        );

        self.child.endInput = self.child.end;
        self.child.searchBoxEnd = new google.maps.places.SearchBox(
          self.child.endInput
        );

        self.child.infoWindow = new google.maps.InfoWindow();
        let pos = {};
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(
            function(position) {
              pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
              };
              self.child.geocoder = new google.maps.Geocoder();
              self.child.geocoder.geocode({ location: pos }, function(
                results,
                status
              ) {
                if (status === "OK") {
                  if (results[0]) {
                    self.setState({ origin: results[0].formatted_address });
                  } else {
                    window.alert("No results found");
                  }
                } else {
                  window.alert("Geocoder failed due to: " + status);
                }
              });
              self.child.infoWindow.open(self.map);
              self.child.map.setCenter(pos);
            },
            function() {
              self.child.handleLocationError(
                true,
                self.child.infoWindow,
                self.child.map.getCenter()
              );
            }
          );
        } else {
          self.handleLocationError(
            false,
            self.child.infoWindow,
            self.child.map.getCenter()
          );
        }
      }
    );
  }

  calculateAndDisplayRoute(
    directionsService,
    directionsDisplay,
    origin,
    destination
  ) {
    let self = this;
    directionsService.route(
      {
        origin,
        destination,
        travelMode: "DRIVING"
      },
      function(response, status) {
        if (status === "OK") {
          directionsDisplay.setDirections(response);
        } else {
          window.alert("Directions request failed due to " + status);
        }
      }
    );
  }

  calculateDistance(origin, destination) {
    let self = this;
    self.child.geocoder = new google.maps.Geocoder();
    self.child.service = new google.maps.DistanceMatrixService();
    self.child.service.getDistanceMatrix(
      {
        origins: [origin],
        destinations: [destination],
        travelMode: "DRIVING",
        avoidHighways: false,
        avoidTolls: false
      },
      function(response, status) {
        if (status !== "OK") {
          alert("Error was: " + status);
        } else {
          var originList = response.originAddresses;
          for (var i = 0; i < originList.length; i++) {
            var results = response.rows[0].elements;

            let distance = results[0].distance.text;
            distance = distance.substr(0, distance.length - 2);
            const price = distance * 2000;
            self.setState({ price });
            self.child.map = new google.maps.Map(self.child.map, {
              zoom: 13,
              center: { lat: -25.363, lng: 131.044 }
            });
            self.calculateAndDisplayRoute(
              self.child.directionsService,
              self.child.directionsDisplay,
              origin,
              destination
            );
            self.child.directionsDisplay.setMap(self.child.map);
          }
        }
      }
    );
  }

  doOrder = async () => {
    setTimeout(this.notify(), 3000);
    try {
      const { origin, destination, price } = this.state;
      const { data } = await order(origin, destination, price);
      setTimeout(
        this.update(
          "Driver Found. Driver Name : " + data.driver_id,
          toast.TYPE.SUCCESS
        ),
        2000
      );
      const socket = socketIOClient("http://localhost:4001");
      socket.emit("order", "Order Found");
    } catch (ex) {
      setTimeout(() => this.update("Driver not Found", toast.TYPE.ERROR), 3000);
    }
  };

  handleToggle = () => {
    const { sidebarStatus } = this.state;
    if (sidebarStatus === "") {
      this.setState({ sidebarStatus: "active" });
    } else {
      this.setState({ sidebarStatus: "" });
    }
  };

  handleOrigin = e => {
    if (e.key === "Enter") {
      let origin = e.target.value;
      this.setState({ origin });
      if (this.state.destination !== "") {
        this.calculateDistance(e.target.value, this.state.destination);
      }
    }
  };

  handleDestination = e => {
    if (e.key === "Enter") {
      let destination = e.target.value;
      this.setState({ destination });
      if (this.state.origin !== "") {
        this.calculateDistance(this.state.origin, e.target.value);
      }
    }
  };

  async cancelOrder() {
    await userCancelOrder();
    window.location = "/user";
  }

  render() {
    return (
      <div className="wrapper">
        <ToastContainer />
        <SideBar
          sidebarStatus={this.state.sidebarStatus}
          sidebar={this.state.sidebar}
          user={this.state.user}
          color={this.state.color}
        />
        <div
          id="content"
          style={{ backgroundColor: "#e4e5e6" }}
          className={this.state.sidebarStatus}
        >
          <Navbar
            onToggle={this.handleToggle}
            color={this.state.color}
            user={this.state.user}
          />
          <Switch>
            <Route path="/user/history" component={UserHistory} />
            <Route
              path="/user/order/current"
              render={props => (
                <UserCurrentOrder
                  {...props}
                  currentOrigin={this.state.currentOrigin}
                  currentDestination={this.state.currentDestination}
                  currentPrice={this.state.currentPrice}
                  onShow={this.showMap}
                  onDelete={this.cancelOrder}
                  ref={node => {
                    this.child = node;
                  }}
                />
              )}
            />
            <Route
              path="/user/order"
              render={props => (
                <UserOrder
                  {...props}
                  origin={this.state.origin}
                  destination={this.state.destination}
                  price={this.state.price}
                  onOrigin={this.handleOrigin}
                  onDestination={this.handleDestination}
                  onOrder={this.doOrder}
                  onShow={this.showMap}
                  ref={node => {
                    this.child = node;
                  }}
                />
              )}
            />
            <Route
              path="/user"
              render={props => (
                <UserProfile
                  {...props}
                  color={this.state.color}
                  user={this.state.user}
                />
              )}
            />
          </Switch>
        </div>
      </div>
    );
  }
}

function loadScript(url, callback) {
  var head = document.getElementsByTagName("head")[0];
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = url;
  script.onreadystatechange = callback;
  script.onload = callback;
  head.appendChild(script);
}

export default UserDashboard;
