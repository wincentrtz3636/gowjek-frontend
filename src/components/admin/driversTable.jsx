import React, { Component } from "react";
import Table from "../common/table";

class DriversTable extends Component {
  columns = [
    { path: "id", label: "Id" },
    { path: "name", label: "Name" },
    { path: "email", label: "Email" },
    { path: "phone", label: "Phone" },
    { path: "status", label: "Status" }
  ];
  render() {
    const { drivers, onSort, sortColumn } = this.props;
    return (
      <Table
        columns={this.columns}
        data={drivers}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default DriversTable;
