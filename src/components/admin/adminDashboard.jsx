import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import SideBar from "../common/Sidebar/sidebar";
import Navbar from "../common/Navbar/navbar";
import DashboardContent from "./dashboardContent";
import "./admin.css";
import UsersContent from "./usersContent";
import {
  getAllDrivers,
  getAllOrders,
  getAllUsers,
  getWeeklyOrderInfo,
  getMonthlyOrderInfo,
  getYearlyOrderInfo,
  getWeeklyUserInfo,
  getWeeklyDriverInfo,
  getMonthlyDriverInfo,
  getYearlyDriverInfo,
  getMonthlyUserInfo,
  getYearlyUserInfo
} from "../../services/admin/adminService";
import DriversContent from "./driversContent";
import OrdersContent from "./ordersContent";

class AdminDashboard extends Component {
  state = {
    user: {
      name: "Admin"
    },
    datas: {
      users: [],
      drivers: [],
      orders: []
    },
    color: {
      navbar: "#3c8dbc",
      navbar_header: "#367fa9"
    },
    sidebar: [
      { label: "Dashboard", path: "/admin" },
      { label: "Users", path: "/admin/user" },
      { label: "Drivers", path: "/admin/driver" },
      { label: "Transactions", path: "/admin/transaction" }
    ],
    sidebarStatus: "",
    dropdownStatus: ""
  };

  async componentDidMount() {
    let { data: users } = await getAllUsers();
    users.bg = "#20a8d8";
    users.icon = "fa fa-users";
    users.label = "Users";

    let { data: drivers } = await getAllDrivers();
    drivers.bg = "#ffc107";
    drivers.icon = "fa fa-motorcycle";
    drivers.label = "Drivers";

    let { data: orders } = await getAllOrders();
    orders.bg = "#4dbd74";
    orders.icon = "fa fa-bitcoin";
    orders.label = "Orders";

    let { data: orderStatistic } = await getWeeklyOrderInfo();
    let { data: userStatistic } = await getWeeklyUserInfo();
    let { data: driverStatistic } = await getWeeklyDriverInfo();

    this.setState({
      datas: {
        users,
        drivers,
        orders
      },
      orderStatistic,
      userStatistic,
      driverStatistic
    });
  }

  handleToggle = () => {
    const { sidebarStatus } = this.state;
    if (sidebarStatus === "") {
      this.setState({ sidebarStatus: "active" });
    } else {
      this.setState({ sidebarStatus: "" });
    }
  };

  handleHover = () => {
    const { dropdownStatus } = this.state;
    if (dropdownStatus === "") {
      this.setState({ dropdownStatus: "show" });
    } else {
      this.setState({ dropdownStatus: "" });
    }
  };

  handleWeeklyOrder = async () => {
    let { data: orderStatistic } = await getWeeklyOrderInfo();
    this.setState({ orderStatistic });
  };

  handleMonthlyOrder = async () => {
    let { data: orderStatistic } = await getMonthlyOrderInfo();
    this.setState({ orderStatistic });
  };

  handleYearlyOrder = async () => {
    let { data: orderStatistic } = await getYearlyOrderInfo();
    this.setState({ orderStatistic });
  };

  handleWeeklyDriver = async () => {
    let { data: driverStatistic } = await getWeeklyDriverInfo();
    this.setState({ driverStatistic });
  };

  handleMonthlyDriver = async () => {
    let { data: driverStatistic } = await getMonthlyDriverInfo();
    this.setState({ driverStatistic });
  };

  handleYearlyDriver = async () => {
    let { data: driverStatistic } = await getYearlyDriverInfo();
    this.setState({ driverStatistic });
  };

  handleWeeklyUser = async () => {
    let { data: userStatistic } = await getWeeklyUserInfo();
    this.setState({ userStatistic });
  };

  handleMonthlyUser = async () => {
    let { data: userStatistic } = await getMonthlyUserInfo();
    this.setState({ userStatistic });
  };

  handleYearlyUser = async () => {
    let { data: userStatistic } = await getYearlyUserInfo();
    this.setState({ userStatistic });
  };

  render() {
    return (
      <div className="wrapper">
        <SideBar
          sidebarStatus={this.state.sidebarStatus}
          sidebar={this.state.sidebar}
          user={this.state.user}
          color={this.state.color}
        />
        <div
          id="content"
          style={{ backgroundColor: "#e4e5e6" }}
          className={this.state.sidebarStatus}
        >
          <Navbar
            onMouseEnter={this.handleHover}
            onToggle={this.handleToggle}
            color={this.state.color}
            user={this.state.user}
          />
          <Switch>
            <Route
              path="/admin/transaction"
              render={props => (
                <OrdersContent {...props} orders={this.state.datas.orders} />
              )}
            />
            <Route
              path="/admin/user"
              render={props => (
                <UsersContent {...props} users={this.state.datas.users} />
              )}
            />
            <Route
              path="/admin/driver"
              render={props => (
                <DriversContent {...props} drivers={this.state.datas.drivers} />
              )}
            />
            <Route
              path="/admin"
              render={props => (
                <DashboardContent
                  {...props}
                  color={this.state.color}
                  datas={this.state.datas}
                  orderStatistic={this.state.orderStatistic}
                  userStatistic={this.state.userStatistic}
                  driverStatistic={this.state.driverStatistic}
                  onWeeklyOrder={this.handleWeeklyOrder}
                  onMonthlyOrder={this.handleMonthlyOrder}
                  onYearlyOrder={this.handleYearlyOrder}
                  onWeeklyUser={this.handleWeeklyUser}
                  onMonthlyUser={this.handleMonthlyUser}
                  onYearlyUser={this.handleYearlyUser}
                  onWeeklyDriver={this.handleWeeklyDriver}
                  onMonthlyDriver={this.handleMonthlyDriver}
                  onYearlyDriver={this.handleYearlyDriver}
                />
              )}
            />
          </Switch>
        </div>
      </div>
    );
  }
}

export default AdminDashboard;
