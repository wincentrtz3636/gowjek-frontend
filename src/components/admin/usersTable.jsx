import React, { Component } from "react";
import Table from "../common/table";

class UsersTable extends Component {
  columns = [
    { path: "id", label: "Id" },
    { path: "name", label: "Name" },
    { path: "email", label: "Email" },
    { path: "phone", label: "Phone" },
    { path: "gopay", label: "GoPay" }
  ];
  render() {
    const { users, onSort, sortColumn } = this.props;
    return (
      <Table
        columns={this.columns}
        data={users}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default UsersTable;
