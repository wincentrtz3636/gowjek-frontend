import React, { Component } from "react";
import CardBody from "../common/cardBody";
import Graph from "../common/graph";

class DashboardContent extends Component {
  render() {
    console.log(this.props);
    return (
      <React.Fragment>
        <div className="container-fluid mt-3">
          <div className="row">
            <div className="col-3">
              <CardBody data={this.props.datas.users} />
            </div>
            <div className="col-3">
              <CardBody data={this.props.datas.drivers} />
            </div>
            <div className="col-3">
              <CardBody data={this.props.datas.orders} />
            </div>
          </div>
          <div className="row mt-3 text-right">
            <Graph
              data={this.props.userStatistic}
              onWeekly={this.props.onWeeklyUser}
              onMonthly={this.props.onMonthlyUser}
              onYearly={this.props.onYearlyUser}
              label={"user"}
              color={"32, 168, 216"}
            />
            <Graph
              data={this.props.orderStatistic}
              onWeekly={this.props.onWeeklyOrder}
              onMonthly={this.props.onMonthlyOrder}
              onYearly={this.props.onYearlyOrder}
              label={"order"}
              color={"77, 189, 116"}
            />

            <Graph
              data={this.props.driverStatistic}
              onWeekly={this.props.onWeeklyDriver}
              onMonthly={this.props.onMonthlyDriver}
              onYearly={this.props.onYearlyDriver}
              label={"driver"}
              color={"255, 193, 7"}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default DashboardContent;
