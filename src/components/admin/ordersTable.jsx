import React, { Component } from "react";
import Table from "../common/table";

class OrdersTable extends Component {
  columns = [
    { path: "id", label: "Id" },
    { path: "start_location", label: "Start Location" },
    { path: "end_location", label: "End Location" },
    { path: "status_id", label: "Order Status" },
    { path: "price", label: "Price" }
  ];
  render() {
    const { orders, onSort, sortColumn } = this.props;
    return (
      <Table
        columns={this.columns}
        data={orders}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default OrdersTable;
