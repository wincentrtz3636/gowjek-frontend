import React, { Component } from "react";
import _ from "lodash";
import UsersTable from "./usersTable";
import SearchBox from "./searchBox";
import Pagination from "../common/pagination";
import { paginate } from "../../utils/paginate";
import OrdersTable from "./ordersTable";

class OrdersContent extends Component {
  state = {
    users: [],
    currentPage: 1,
    pageSize: 4,
    searchQuery: "",
    sortColumn: { path: "title", order: "asc" }
  };

  componentDidMount() {}

  getPagedData = () => {
    const {
      pageSize,
      currentPage,
      sortColumn,
      selectedGenre,
      searchQuery
    } = this.state;
    const allOrders = this.props.orders;
    let filtered = allOrders;
    if (searchQuery)
      filtered = allOrders.filter(m =>
        m.start_location.toLowerCase().startsWith(searchQuery.toLowerCase())
      );

    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);

    const orders = paginate(sorted, currentPage, pageSize);

    return { totalCount: filtered.length, data: orders };
  };

  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  handleSearch = query => {
    this.setState({ searchQuery: query, currentPage: 1 });
  };

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  render() {
    const { totalCount, data: orders } = this.getPagedData();
    return (
      <div className="container-fluid">
        <SearchBox
          value={this.state.searchQuery}
          onChange={this.handleSearch}
        />
        <OrdersTable
          orders={orders}
          sortColumn={this.state.sortColumn}
          onSort={this.handleSort}
        />
        <Pagination
          itemsCount={totalCount}
          pageSize={this.state.pageSize}
          currentPage={this.state.currentPage}
          onPageChange={this.handlePageChange}
        />
      </div>
    );
  }
}

export default OrdersContent;
