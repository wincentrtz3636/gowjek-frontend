/*global google*/

import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import SideBar from "../common/Sidebar/sidebar";
import Navbar from "../common/Navbar/navbar";
import { toast, ToastContainer } from "react-toastify";
import { css } from "glamor";
import socketIOClient from "socket.io-client";

import {
  getDriverInfo,
  getDriverOrders,
  updateDriverStatus,
  updateOrderStatus,
  driverCancelOrder
} from "../../services/driver/driverService";

import DriverHistory from "./driverHistory";
import DriverDashboardContent from "./driverDashboardContent";
import DriverOrder from "./driverOrder";

import "./driver.css";

class DriverDashboard extends Component {
  state = {
    user: [],
    orders: [],
    color: {
      navbar: "#8cbd47",
      navbar_header: "#82aa49"
    },
    sidebar: [
      { label: "Availibity", path: "", content: "true" },
      { label: "Dashboard", path: "/driver" },
      { label: "Order", path: "/driver/order" },
      { label: "History", path: "/driver/history" }
    ],
    sidebarStatus: "",
    origin: "",
    destination: "",
    price: ""
  };

  notify = label =>
    (this.toastId = toast(label, {
      type: "info",
      autoClose: false
    }));

  update = (label, type) =>
    toast.update(this.toastId, {
      render: label,
      type: type,
      className: css({
        transform: "rotateY(360deg)",
        transition: "transform 0.6s"
      }),
      autoClose: 8000
    });

  constructor(props) {
    super(props);
    this.updateDriverStatus = this.updateDriverStatus.bind(this);
    this.showMap = this.showMap.bind(this);
    this.updateOrderStatus = this.updateOrderStatus.bind(this);
  }

  async componentDidMount() {
    const { data: user } = await getDriverInfo();
    const { data: orders } = await getDriverOrders();
    if (orders.length !== 0) {
      if (orders[0].status_id < 3) {
        this.setState({
          origin: orders[0].start_location,
          destination: orders[0].end_location,
          price: orders[0].price
        });
      }
    }

    this.setState({ user, orders });
  }

  handleToggle = () => {
    const { sidebarStatus } = this.state;
    if (sidebarStatus === "") {
      this.setState({ sidebarStatus: "active" });
    } else {
      this.setState({ sidebarStatus: "" });
    }
  };

  async updateDriverStatus() {
    setTimeout(this.notify("Updating Status..."), 3000);
    try {
      let user = { ...this.state.user };
      user.status = !user.status;
      this.setState({ user });
      await updateDriverStatus();
      setTimeout(
        this.update("Driver Status Updated", toast.TYPE.SUCCESS),
        2000
      );
      if (user.status) {
        const socket = socketIOClient("http://localhost:4001");
        socket.emit("order");
      }
    } catch (ex) {
      setTimeout(() => this.update("Update Fail", toast.TYPE.ERROR), 3000);
    }
  }

  showMap() {
    let self = this;
    loadScript(
      "https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyByH0c5bxYDZ48BLQ401BBsm4DppG6QNkQ&libraries=places",
      function() {
        self.child.directionsService = new google.maps.DirectionsService();
        self.child.directionsDisplay = new google.maps.DirectionsRenderer();
        self.child.map = new google.maps.Map(self.child.map, {
          zoom: 13,
          center: { lat: -25.363, lng: 131.044 }
        });

        self.child.infoWindow = new google.maps.InfoWindow();
        let pos = {};
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            self.child.infoWindow.open(self.map);
            self.child.map.setCenter(pos);
          });
        } else {
          self.handleLocationError(
            false,
            self.child.infoWindow,
            self.child.map.getCenter()
          );
        }

        self.calculateAndDisplayRoute(
          self.child.directionsService,
          self.child.directionsDisplay
        );
        self.child.directionsDisplay.setMap(self.child.map);
      }
    );
  }

  calculateAndDisplayRoute(directionsService, directionsDisplay) {
    let self = this;

    directionsService.route(
      {
        origin: self.state.origin,
        destination: self.state.destination,
        travelMode: "DRIVING"
      },
      function(response, status) {
        if (status === "OK") {
          directionsDisplay.setDirections(response);
        } else {
          window.alert("Directions request failed due to " + status);
        }
      }
    );
  }

  async updateOrderStatus() {
    setTimeout(this.notify("Updating Status..."), 3000);
    try {
      await updateOrderStatus();
      setTimeout(
        this.update("Driver Status Updated", toast.TYPE.SUCCESS),
        2000
      );
      if (this.state.orders[0].status_id < 3) {
        let orders = { ...this.state.orders };
        orders.status_id = Number(orders.status_id) + 1;
        this.setState({ orders });
      }
    } catch (ex) {
      setTimeout(() => this.update("Update Fail", toast.TYPE.ERROR), 3000);
    }
  }

  async cancelOrder() {
    await driverCancelOrder();
    window.location = "/driver";
  }

  componentWillUnmount() {
    const socket = socketIOClient("http://localhost:4001");
    socket.disconnected;
  }
  render() {
    const socket = socketIOClient("http://localhost:4001");
    socket.on("order", label => {
      this.notify(label);
    });
    return (
      <div className="wrapper">
        <ToastContainer />
        <SideBar
          sidebarStatus={this.state.sidebarStatus}
          sidebar={this.state.sidebar}
          user={this.state.user}
          color={this.state.color}
          onToggle={this.updateDriverStatus}
        />
        <div
          id="content"
          style={{ backgroundColor: "#e4e5e6" }}
          className={this.state.sidebarStatus}
        >
          <Navbar
            onToggle={this.handleToggle}
            color={this.state.color}
            user={this.state.user}
          />
          <Switch>
            <Route path="/driver/history" component={DriverHistory} />
            <Route
              path="/driver/order"
              render={props => (
                <DriverOrder
                  {...props}
                  origin={this.state.origin}
                  destination={this.state.destination}
                  price={this.state.price}
                  onShow={this.showMap}
                  onUpdated={this.updateOrderStatus}
                  onCancel={this.cancelOrder}
                  ref={node => {
                    this.child = node;
                  }}
                />
              )}
            />
            <Route
              path="/driver"
              render={props => (
                <DriverDashboardContent
                  {...props}
                  driver={this.state.user}
                  ordersCanceled={this.state.orders.filter(
                    m => m.status_id === "4"
                  )}
                  ordersSuccess={this.state.orders.filter(
                    m => m.status_id === "3"
                  )}
                />
              )}
            />
          </Switch>
        </div>
      </div>
    );
  }
}

function loadScript(url, callback) {
  var head = document.getElementsByTagName("head")[0];
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = url;
  script.onreadystatechange = callback;
  script.onload = callback;
  head.appendChild(script);
}

export default DriverDashboard;
