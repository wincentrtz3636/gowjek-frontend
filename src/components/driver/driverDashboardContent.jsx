import React, { Component } from "react";
import CardBody from "../common/cardBody";
import { Line } from "react-chartjs-2";

class DriverDashboardContent extends Component {
  state = {
    data: [
      {
        label: "Completed Order",
        bg: "#4dbd74",
        icon: "fa fa-motorcycle"
      },
      {
        label: "Canceled Order",
        bg: "#f86c6b",
        icon: "fa fa-exclamation-triangle"
      }
    ]
  };

  render() {
    const data = canvas => {
      const ctx = canvas.getContext("2d");
      const gradient = ctx.createLinearGradient(0, 0, 100, 0);

      return {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [
          {
            label: "# of Users",
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: "rgba(32, 168, 216,1)",
            borderColor: "rgba(32, 168, 216,1)",
            borderWidth: 1
          }
        ],
        backgroundColor: gradient
      };
    };
    return (
      <React.Fragment>
        <div className="container-fluid">
          <div className="row mt-3">
            <div
              key={this.state.data[0].label}
              className="col-sm-12 col-md-6 col-lg-3 mb-3"
            >
              <CardBody
                data={this.state.data[0]}
                count={this.props.ordersSuccess.length}
              />
            </div>
            <div
              key={this.state.data[1].label}
              className="col-sm-12 col-md-6 col-lg-3 mb-3"
            >
              <CardBody
                data={this.state.data[1]}
                count={this.props.ordersCanceled.length}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-md-6">
              <Line data={data} />
            </div>
            <div className="col-12 col-md-6">
              <Line data={data} />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default DriverDashboardContent;
