/*global google*/

import React, { Component } from "react";

class DriverOrder extends Component {
  componentDidMount() {
    this.props.onShow();
  }

  render() {
    const mapStyle = {
      width: "500",
      height: "850px"
    };
    return (
      <div>
        <div>
          <div className="form-group current-order col-5">
            <div
              className="row container-fluid mt-2"
              id=""
              style={{ padding: "15px" }}
            >
              <div className="col-4">Logo</div>
              <div className="col-4 text-center">Order Number</div>
              <div className="col-4 text-center">Customer Name</div>
            </div>
            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">
                  <i
                    style={{ color: "#8cbd47" }}
                    className="fa fa-map-marker"
                  />
                </span>
              </div>
              <input
                className="form-control col-12 search-input"
                id="start-input"
                ref={node => {
                  this.start = node;
                }}
                defaultValue={this.props.origin}
                disabled
              />
            </div>
            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">
                  <i
                    style={{ color: "#edb541" }}
                    className="fa fa-map-marker"
                  />
                </span>
              </div>
              <input
                className="form-control col-12 search-input"
                id="end-input"
                ref={node => {
                  this.end = node;
                }}
                defaultValue={this.props.destination}
                disabled
              />
            </div>

            <div
              className="row container-fluid"
              id="order-information"
              style={{ padding: "15px" }}
            >
              <div className="col-4">
                <h4 style={{ margin: 0 }}>IDR {this.props.price}</h4>
              </div>
              <div className="col-4 text-right">
                <button
                  className="btn btn-success mr-2"
                  onClick={this.props.onUpdated}
                >
                  Accept Order
                </button>
              </div>
              <div className="col-4 text-right">
                <button
                  className="btn btn-danger mr-2"
                  onClick={this.props.onCancel}
                >
                  CANCELLED ORDER
                </button>
              </div>
            </div>
          </div>

          <div
            ref={node => {
              this.map = node;
            }}
            style={mapStyle}
          />
        </div>
      </div>
    );
  }
}
export default DriverOrder;
