import React, { Component } from "react";
import { getDriverOrders } from "../../services/driver/driverService";
import HistoryList from "../common/historyList";

class DriverHistory extends Component {
  state = {
    histories: []
  };
  async componentDidMount() {
    try {
      const { data } = await getDriverOrders();
      data.forEach(d => {
        d.date = this.formatDate(d);
      });
      this.setState({ histories: data });
    } catch (ex) {
      // if (ex.response && ex.response.status === 400) {
      //   const errors = { ...this.state.errors };
      //   errors.username = ex.response.data;
      //   this.setState({ errors });
      // }
    }
  }
  formatDate(data) {
    const dateFullFormat = new Date(data.created_at);
    const monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
    const minutes =
      dateFullFormat.getMinutes() < 10
        ? "0" + dateFullFormat.getMinutes()
        : dateFullFormat.getMinutes();
    const times = dateFullFormat.getHours() + ":" + minutes;
    const date =
      dateFullFormat.getDate() + " " + monthNames[dateFullFormat.getMonth()];
    return date + ", " + times;
  }
  render() {
    return (
      <div className="history-container">
        {this.state.histories.map(history => (
          <HistoryList key={history.id} history={history} />
        ))}
        <div className="container history-list">
          <div className="row">
            <div
              className="col-1 history-status"
              style={{ backgroundColor: "#f86c6b" }}
            >
              <h2>
                <i className="fa fa-times" />
              </h2>
            </div>
            <div className="col">
              <div className="row">
                <div className="col-12">Central Park</div>
                <div className="col-12">9 September, 17:51</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DriverHistory;
