import http from "../httpServices";
import config from "../../config.json";

export function register(user) {
  return http.post(config.apiUrl + "/admin/register", {
    name: user.name,
    email: user.email,
    password: user.password,
    password_confirmation: user.password
  });
}

export function activeUserAccount(userId) {
  return http.patch(config.apiUrl + "/admin/user/" + userId + "/activate", {});
}

export function deactiveUserAccount(userId) {
  return http.patch(
    config.apiUrl + "/admin/user/" + userId + "/deactivate",
    {}
  );
}

export function activeDriverAccount(driverId) {
  return http.patch(
    config.apiUrl + "/admin/driver/" + driverId + "/activate",
    {}
  );
}

export function deactiveDriverAccount(driverId) {
  return http.patch(
    config.apiUrl + "/admin/driver/" + driverId + "/deactivate",
    {}
  );
}

export function getAllUsers() {
  return http.get(config.apiUrl + "/admin/users");
}

export function getAllDrivers() {
  return http.get(config.apiUrl + "/admin/drivers");
}

export function getAllOrders() {
  return http.get(config.apiUrl + "/admin/orders");
}

export function getWeeklyOrderInfo() {
  return http.get(config.apiUrl + "/admin/orders/weekly");
}

export function getMonthlyOrderInfo() {
  return http.get(config.apiUrl + "/admin/orders/monthly");
}

export function getYearlyOrderInfo() {
  return http.get(config.apiUrl + "/admin/orders/yearly");
}

export function getWeeklyUserInfo() {
  return http.get(config.apiUrl + "/admin/users/weekly");
}

export function getMonthlyUserInfo() {
  return http.get(config.apiUrl + "/admin/users/monthly");
}

export function getYearlyUserInfo() {
  return http.get(config.apiUrl + "/admin/users/yearly");
}

export function getWeeklyDriverInfo() {
  return http.get(config.apiUrl + "/admin/drivers/weekly");
}

export function getMonthlyDriverInfo() {
  return http.get(config.apiUrl + "/admin/drivers/monthly");
}

export function getYearlyDriverInfo() {
  return http.get(config.apiUrl + "/admin/drivers/yearly");
}
