import http from "../httpServices";
import config from "../../config.json";

export function register(user) {
  return http.post(config.apiUrl + "/user/register", {
    name: user.name,
    email: user.email,
    phone: user.phone,
    password: user.password,
    password_confirmation: user.password
  });
}

export function order(origin, destination, price) {
  return http.post(config.apiUrl + "/order", {
    start_location: origin,
    end_location: destination,
    price: price,
    payment_id: 1
  });
}

export function getUserInfo() {
  return http.get(config.apiUrl + "/user");
}

export function getUserOrders() {
  return http.get(config.apiUrl + "/user/orders");
}

export function userCancelOrder() {
  return http.patch(config.apiUrl + "/order/user/cancel");
}
