import axios from "axios";
// import logger from "./logService";
import { toast } from "react-toastify";

axios.interceptors.response.use(null, error => {
  const expectedError =
    error.response &&
    error.response.status >= 400 &&
    error.response.status <= 500;

  if (!expectedError) {
    toast.error("An unexpected error occurred");
  }
  return Promise.reject(error);
});

function setJwt(jwt) {
  if (jwt) {
    axios.defaults.headers.common["Authorization"] = jwt;
  }
}

export default {
  get: axios.get,
  post: axios.post,
  patch: axios.patch,
  delete: axios.delete,
  setJwt
};
