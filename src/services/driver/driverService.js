import http from "../httpServices";
import config from "../../config.json";

export function register(user) {
  return http.post(config.apiUrl + "/driver/register", {
    name: user.name,
    email: user.email,
    phone: user.phone,
    vehicle: user.vehicle,
    password: user.password,
    password_confirmation: user.password
  });
}

export function getDriverInfo() {
  return http.get(config.apiUrl + "/driver");
}

export function getDriverOrders() {
  return http.get(config.apiUrl + "/driver/orders");
}

export function updateDriverStatus() {
  return http.patch(config.apiUrl + "/driver/update/status", {});
}

export function updateDriverLocation(location) {
  return http.patch(config.apiUrl + "/driver/update/location", {
    location
  });
}

export function updateOrderStatus() {
  return http.patch(config.apiUrl + "/order/update/status");
}

export function driverCancelOrder() {
  return http.patch(config.apiUrl + "/order/driver/cancel");
}
